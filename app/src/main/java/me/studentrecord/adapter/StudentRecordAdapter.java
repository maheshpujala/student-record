package me.studentrecord.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import me.studentrecord.R;
import me.studentrecord.model.Student;
import me.studentrecord.databinding.StudentRecordItemBinding;

public class StudentRecordAdapter extends RecyclerView.Adapter<StudentRecordAdapter.StudentRecordViewHolder> {

    private final LayoutInflater mInflater;
    private List<Student> mStudents;

    public StudentRecordAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @NonNull
    @Override
    public StudentRecordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        StudentRecordItemBinding studentRecordItemBinding = DataBindingUtil.inflate(mInflater, R.layout.student_record_item, parent, false);
        return new StudentRecordViewHolder(studentRecordItemBinding);
    }

    @Override
    public void onBindViewHolder(StudentRecordViewHolder holder, int position) {
        holder.studentRecordItemBinding.setStudent(mStudents.get(position));
    }

    public void setStudentRecords(List<Student> students){
        mStudents = students;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mStudents != null)
            return mStudents.size();
        else return 0;
    }


    class StudentRecordViewHolder extends RecyclerView.ViewHolder {
        StudentRecordItemBinding studentRecordItemBinding;
        StudentRecordViewHolder(StudentRecordItemBinding studentRecordItemBinding) {
            super(studentRecordItemBinding.getRoot());
            this.studentRecordItemBinding = studentRecordItemBinding;
        }
    }
}
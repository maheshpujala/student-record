package me.studentrecord.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface StudentDao {
    @Query("SELECT * FROM student_table")
    LiveData<List<Student>> getAllStudents();

    @Query("SELECT * FROM student_table WHERE id IN (:studentIds)")
    List<Student> loadAllByIds(int[] studentIds);

    @Query("SELECT * FROM student_table WHERE name LIKE :name LIMIT 1")
    Student findByName(String name);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Student student);

    @Delete
    void delete(Student student);

    @Query("DELETE FROM student_table")
    void deleteAll();

}

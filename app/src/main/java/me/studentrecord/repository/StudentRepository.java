package me.studentrecord.repository;


import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import me.studentrecord.model.Student;
import me.studentrecord.model.StudentDao;
import me.studentrecord.model.StudentDatabase;

public class StudentRepository {

    private StudentDao mStudentDao;
    private LiveData<List<Student>> mAllStudents;

    public StudentRepository(Application application) {
        StudentDatabase db = StudentDatabase.getDatabase(application);
        mStudentDao = db.studentDao();
        mAllStudents = mStudentDao.getAllStudents();
    }

    public LiveData<List<Student>> getAllStudents() {
        return mAllStudents;
    }

    public void insert(Student student) {
        StudentDatabase.databaseWriteExecutor.execute(() -> mStudentDao.insert(student));
    }
}

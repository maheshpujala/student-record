package me.studentrecord.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;

import me.studentrecord.R;
import me.studentrecord.adapter.StudentRecordAdapter;
import me.studentrecord.databinding.ActivityMainBinding;
import me.studentrecord.viewmodel.StudentViewModel;

public class MainActivity extends AppCompatActivity {
private StudentViewModel mStudentViewModel;
private ActivityMainBinding activityMainBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(MainActivity.this,R.layout.activity_main);
        mStudentViewModel = new ViewModelProvider(this).get(StudentViewModel.class);


        final StudentRecordAdapter adapter = new StudentRecordAdapter(this);
        activityMainBinding.rvStudentRecord.setAdapter(adapter);
        activityMainBinding.rvStudentRecord.setLayoutManager(new LinearLayoutManager(this));

        mStudentViewModel.getAllStudents().observe(MainActivity.this, adapter::setStudentRecords);
    }
}

package me.studentrecord.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import me.studentrecord.model.Student;
import me.studentrecord.repository.StudentRepository;

public class StudentViewModel extends AndroidViewModel {

    private StudentRepository mStudentRepository;

    private LiveData<List<Student>> mAllStudents;

    public StudentViewModel (Application application) {
        super(application);
        mStudentRepository = new StudentRepository(application);
        mAllStudents = mStudentRepository.getAllStudents();
    }

    public LiveData<List<Student>> getAllStudents() { return mAllStudents; }

    public void insert(Student student) { mStudentRepository.insert(student); }
}